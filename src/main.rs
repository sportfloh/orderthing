mod eventtypelist;

use fltk::{app, button::Button, enums::Color, prelude::*, window::Window};
use orderthing::db::DatabaseManager;

#[derive(Debug, Clone, Copy)]
enum Message {}

#[derive(Default, Debug, Clone)]
struct AppState {}

pub fn main() -> anyhow::Result<(), FltkError> {
    let mut db_manager = DatabaseManager::new();
    let results = db_manager.load(orderthing::db::schema::eventtypes::dsl::eventtypes);
    dbg!(&results);

    let app = app::App::default();
    let mut window = Window::default()
        .with_size(app::screen_size().0 as i32, app::screen_size().1 as i32)
        .center_screen()
        .with_label("OrderThing");
    window.set_color(Color::White);
    window.set_callback(|_| {
        if fltk::app::event() == fltk::enums::Event::Close {
            app::quit(); // Which would close using the close button. You can also assign other keys to close the application
        }
    });
    let _ = Button::new(160, 210, 80, 40, "Click me!");
    window.end();
    window.show();

    eventtypelist::show_eventtype_list(&app, &results);

    app.run().unwrap();
    Ok(())
}
