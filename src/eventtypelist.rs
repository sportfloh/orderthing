use fltk::{
    app,
    app::App,
    button::Button,
    enums,
    enums::{Color, Shortcut},
    group,
    group::{Group, Pack, Scroll},
    menu::{MenuBar, MenuFlag},
    prelude::*,
    window::OverlayWindow,
};
use orderthing::db::models::Eventtype;
use std::ops::{Deref, DerefMut};

#[derive(Debug, Clone)]
pub enum EventTypeWindowMessage {
    Edit,
}

#[derive(Default, Debug, Clone)]
pub struct EventTypeList {
    pack: Pack,
}

impl EventTypeList {
    pub fn new(s: app::Sender<EventTypeWindowMessage>, eventtypes: &[Eventtype]) -> EventTypeList {
        let mut pack = Pack::default().size_of_parent();
        pack.set_color(Color::White);

        let mut menubar = MenuBar::default();
        menubar.set_size(pack.width(), 30);
        menubar.set_color(Color::White);
        menubar.add_emit(
            "&Edit",
            Shortcut::empty(),
            MenuFlag::Normal,
            s,
            EventTypeWindowMessage::Edit,
        );
        menubar.end();

        let mut scroll = Scroll::default().size_of_parent();
        scroll.set_color(Color::White);
        scroll.set_type(group::ScrollType::Vertical);

        let mut et_list = Pack::default().size_of_parent();
        et_list.set_color(Color::White);

        for et in eventtypes {
            let mut group = Group::default().with_size(et_list.width(), 50);
            group.set_frame(enums::FrameType::BorderFrame);
            group.set_align(enums::Align::Center);
            let label = et.name.to_string();
            group.set_label(&label);
            group.set_callback(move |_| {
                dbg!(&label);
            });
            let mut edit_button = Button::new(group.width() - 120, 10, 50, 30, "Edit");
            edit_button.set_callback(move |_| {
                dbg!("Edit");
            });
            let mut delete_button = Button::new(group.width() - 60, 10, 50, 30, "Delete");
            delete_button.set_callback(move |_| {
                dbg!("Delete");
            });
            group.handle(|g, ev| match ev {
                enums::Event::Push => {
                    g.do_callback();
                    true
                }
                _ => false,
            });
            group.end();
        }
        et_list.end();
        scroll.end();
        pack.end();
        EventTypeList { pack }
    }
}

impl Deref for EventTypeList {
    type Target = Pack;
    fn deref(&self) -> &Self::Target {
        &self.pack
    }
}

impl DerefMut for EventTypeList {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.pack
    }
}

pub fn show_eventtype_list(app: &App, eventtypes: &[Eventtype]) {
    let (s, r) = app::channel::<EventTypeWindowMessage>();

    let mut ow = OverlayWindow::default().with_size(500, 500).center_screen();
    ow.set_color(Color::White);
    ow.make_modal(true);

    let _ = EventTypeList::new(s, eventtypes);
    ow.end();
    ow.show();

    while app.wait() {
        if let Some(msg) = r.recv() {
            match msg {
                EventTypeWindowMessage::Edit => {
                    dbg!("Edit");
                }
            }
        }
    }
}
