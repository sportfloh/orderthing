// Generated by diesel_ext

#![allow(unused)]
#![allow(clippy::all)]

use chrono::NaiveDateTime;
#[derive(Queryable, Debug)]
pub struct Event {
    pub id: i32,
    pub created: NaiveDateTime,
    pub name: String,
    pub eventtype_id: i32,
}

#[derive(Queryable, Debug)]
pub struct EventtypeProduct {
    pub id: i32,
    pub price: f32,
    pub eventtype_id: i32,
}

#[derive(Queryable, Debug)]
pub struct Eventtype {
    pub id: i32,
    pub created: NaiveDateTime,
    pub name: String,
}

#[derive(Queryable, Debug)]
pub struct OrderProduct {
    pub id: i32,
    pub product_id: i32,
}

#[derive(Queryable, Debug)]
pub struct Order {
    pub id: i32,
    pub created: NaiveDateTime,
    pub started: Option<NaiveDateTime>,
    pub ready: Option<NaiveDateTime>,
    pub payed: Option<NaiveDateTime>,
    pub event_id: i32,
    pub order_products_id: i32,
}

#[derive(Queryable, Debug)]
pub struct Product {
    pub id: i32,
    pub created: NaiveDateTime,
    pub name: String,
    pub alcoholic: bool,
    pub eventtype_products_id: i32,
}
