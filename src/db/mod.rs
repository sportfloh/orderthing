pub mod models;
pub mod schema;

use diesel::prelude::*;
use diesel::query_builder::InsertStatement;
use diesel::query_dsl::methods::{ExecuteDsl, LoadQuery};
use diesel_migrations::{embed_migrations, EmbeddedMigrations, MigrationHarness};
use std::env;

pub struct DatabaseManager {
    connection: SqliteConnection,
}

impl DatabaseManager {
    const DATABASE_URL: &str = "ORDERTHING_DATABASE_URL";

    /// Creates a new DatabaseManager instance
    ///
    /// # Examples
    ///```
    /// use orderthing::db::DatabaseManager;
    ///
    /// let db_manager = DatabaseManager::new();
    /// ```
    pub fn new() -> Self {
        const MIGRATIONS: EmbeddedMigrations = embed_migrations!();

        let mut connection = Self::establish_connection();
        connection
            .run_pending_migrations(MIGRATIONS)
            .expect("Failed to run diesel migrations");

        Self { connection }
    }

    /// Establish a connection to a database
    fn establish_connection() -> SqliteConnection {
        dotenvy::dotenv().ok();

        let database_url = env::var(Self::DATABASE_URL).expect("DATABASE_URL must be set");
        SqliteConnection::establish(&database_url)
            .unwrap_or_else(|_| panic!("Error connecting to {}", database_url))
    }

    pub fn load<T, U>(&mut self, table: T) -> Vec<U>
    where
        T: Table + for<'a> LoadQuery<'a, SqliteConnection, U>,
    {
        table
            .load::<U>(&mut self.connection)
            .expect("Error loading eventtypes")
    }

    pub fn insert_into_table<T, M>(&mut self, table: T, records: M)
    where
        T: Table,
        M: diesel::Insertable<T>,
        InsertStatement<T, M::Values>: ExecuteDsl<SqliteConnection>,
    {
        diesel::insert_into(table)
            .values(records)
            .execute(&mut self.connection)
            .unwrap();
    }
}

impl Default for DatabaseManager {
    fn default() -> Self {
        Self::new()
    }
}

#[cfg(test)]
mod tests {
    use super::DatabaseManager;
    use std::path::{Path, PathBuf};
    use testdir::testdir;

    #[test]
    fn test_create_db() {
        let _: PathBuf = testdir!();
        let _ = Path::new(DatabaseManager::DATABASE_URL);
    }
}
