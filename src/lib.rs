#[macro_use]
extern crate diesel;

#[cfg(test)]
#[macro_use]
extern crate galvanic_assert;

pub mod db;

#[cfg(test)]
mod tests {}
