CREATE TABLE IF NOT EXISTS products(
    id INTEGER PRIMARY KEY NOT NULL UNIQUE,
    created DATETIME DEFAULT current_timestamp NOT NULL,
    name TEXT NOT NULL,
    alcoholic BOOLEAN NOT NULL,
    eventtype_products_id INTEGER NOT NULL,
    FOREIGN KEY (eventtype_products_id) REFERENCES eventtype_products(id)
);
