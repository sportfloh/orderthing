CREATE TABLE IF NOT EXISTS orders(
    id INTEGER PRIMARY KEY NOT NULL UNIQUE,
    created DATETIME DEFAULT current_timestamp NOT NULL,
    started DATETIME,
    ready DATETIME,
    payed DATETIME,
    event_id INTEGER NOT NULL,
    order_products_id INTEGER NOT NULL,
    FOREIGN KEY (event_id) REFERENCES events(id),
    FOREIGN KEY (order_products_id) REFERENCES order_products(id)
);
