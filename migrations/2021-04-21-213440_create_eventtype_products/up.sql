CREATE TABLE IF NOT EXISTS eventtype_products(
    id INTEGER PRIMARY KEY NOT NULL UNIQUE,
    price REAL NOT NULL,
    eventtype_id INTEGER NOT NULL,
    FOREIGN KEY (eventtype_id) REFERENCES eventtypes(id)
);
