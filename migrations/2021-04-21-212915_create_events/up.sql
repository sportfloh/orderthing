CREATE TABLE IF NOT EXISTS events(
    id INTEGER PRIMARY KEY NOT NULL UNIQUE,
    created DATETIME DEFAULT current_timestamp NOT NULL,
    name TEXT NOT NULL,
    eventtype_id INTEGER NOT NULL,
    FOREIGN KEY (eventtype_id) REFERENCES eventtypes(id)
);
