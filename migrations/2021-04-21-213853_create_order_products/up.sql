CREATE TABLE IF NOT EXISTS order_products(
    id INTEGER PRIMARY KEY NOT NULL UNIQUE,
    product_id INTEGER NOT NULL,
    FOREIGN KEY (product_id) REFERENCES products(id)
);
